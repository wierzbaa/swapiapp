﻿using SwapiApp.Models;
using Xunit;

namespace SwapiApp.Tests
{
    public class PropertyNameHelperTests
    {
        [Fact]
        public void FilmTitleProperty_IsEqualAsExpected()
        {
            var expected = "Title";
            Assert.Equal(expected, PropertyNameHelper.FilmTitleProperty);
        }

        [Fact]
        public void StarshipTitleProperty_IsEqualAsExpected()
        {
            var expected = "Name";
            Assert.Equal(expected, PropertyNameHelper.StarshipsNameProperty);
        }

        [Fact]
        public void VehicleTitleProperty_IsEqualAsExpected()
        {
            var expected = "Name";
            Assert.Equal(expected, PropertyNameHelper.VehicleNameProperty);
        }
    }
}
