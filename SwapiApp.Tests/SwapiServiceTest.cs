﻿using Microsoft.Extensions.Logging;
using Moq;
using SwapiApp.Interfaces;
using SwapiApp.Models;
using SwapiApp.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace SwapiApp.Tests
{
    public class SwapiServiceTest
    {

        private readonly ISwapiService _swapiService;
        private readonly Mock<ILogger<SwapiService>> _loggerMock;
        private readonly Mock<IHttpClientService> _httpClientService;

        public SwapiServiceTest()
        {
            _loggerMock = new Mock<ILogger<SwapiService>>();
            _httpClientService = new Mock<IHttpClientService>();
            _swapiService = new SwapiService(_httpClientService.Object, _loggerMock.Object);
        }

        [Theory]
        [ClassData(typeof(EmptyTextDataHelper))]
        public void GetPersonByName_WhenGivenNameIsEmpty_ThenExceptionIsThrown(string text)
        {
            Assert.ThrowsAsync<ArgumentException>(() => _swapiService.GetPersonByName(text));
        }

        [Fact]
        public void GetPersonByName_WhenThereIsResultWithGivenNameIsEmpty_ThenExceptionIsThrown()
        {
            var name = "NaPewnoTakiegoNieBedzie";

            _httpClientService.Setup(mock => mock.GetAsync<PeopleSearchResult>(It.IsAny<string>()))
                  .Returns(Task.FromResult(new PeopleSearchResult()));

            Assert.ThrowsAsync<Exception>(() => _swapiService.GetPersonByName(name));
        }

        [Fact]
        public void GetPersonByName_WhenThereIsResultWithGivenNameIsNull_ThenExceptionIsThrown()
        {
            var name = "NaPewnoTakiegoNieBedzie";

            _httpClientService.Setup(mock => mock.GetAsync<PeopleSearchResult>(It.IsAny<string>()))
                  .Returns(Task.FromResult((PeopleSearchResult)null));

            Assert.ThrowsAsync<Exception>(() => _swapiService.GetPersonByName(name));
        }

        [Fact]
        public async Task GetPersonByName_WhenThereIsResultWithGivenName_ReturnsPeople()
        {
            var name = "Luke";
            var expected = new People() { Name = name };

            _httpClientService.Setup(mock => mock.GetAsync<PeopleSearchResult>(It.IsAny<string>()))
                  .Returns(Task.FromResult(new PeopleSearchResult()
                  {
                      Results = new List<People>() { expected }
                  }));

            var result = await _swapiService.GetPersonByName(name);

            Assert.Equal(expected, result);
        }

        [Fact]
        public async Task GetNames_WhenUrlsListIsEmpty_ReturnEmptyList()
        {
            var urls = new List<Uri>();
            var result = await _swapiService.GetNames<object>(urls, "Title", It.IsAny<string>());
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetNames_WhenUrlsListIsNull_ReturnEmptyList()
        {
            var result = await _swapiService.GetNames<object>(null, "Title", It.IsAny<string>());
            Assert.Empty(result);
        }

        [Theory]
        [MemberData(nameof(TypesWithCorrectPropertyExample))]
        public async Task GetNames_WhenThereIsFilmWithGivenName_ReturnListOfFilmNames<T>(T expected, string propertyName)
        {
            var urls = new List<Uri>() { new Uri("https://swapi.dev/api/films/1/") };

            _httpClientService.Setup(mock => mock.GetAsync<T>(It.IsAny<string>()))
                     .Returns(Task.FromResult(expected));

            var result = await _swapiService.GetNames<T>(urls, propertyName, It.IsAny<string>());
            Assert.NotEmpty(result);
        }

        [Theory]
        [MemberData(nameof(TypesExample))]
        public async Task GetNames_WhenThereIsNoFilmWithGivenName_ThenListOfNamesDoesntChange<T>(T type)
        {
            var names = new List<string>();
            var urls = new List<Uri>() { new Uri("https://swapi.dev/api/films/1/") };

            _httpClientService.Setup(mock => mock.GetAsync<T>(It.IsAny<string>()))
                     .Returns(Task.FromResult(default(T)));

            var result = await _swapiService.GetNames<T>(urls, "Any", It.IsAny<string>());

            Assert.Equal(names, result);
        }

        [Theory]
        [MemberData(nameof(TypesWithInCorrectPropertyExample))]
        public void PropertyNameExist_WhenThereIsNoPropertyNameInObject_ThenReturnFalse(object obj, string propertyName)
        {
            Assert.False(_swapiService.PropertyNameExist(obj, propertyName));
        }

        [Theory]
        [MemberData(nameof(TypesWithCorrectPropertyExample))]
        public void PropertyNameExist_WhenPropertyNameExistInObject_ThenReturnTrue(object obj, string propertyName)
        {
            Assert.True(_swapiService.PropertyNameExist(obj, propertyName));
        }

        public static IEnumerable<object[]> TypesExample()
        {
            yield return new object[] { typeof(Film) };
            yield return new object[] { typeof(Vehicle) };
            yield return new object[] { typeof(Starship) };
        }

        public static IEnumerable<object[]> TypesWithCorrectPropertyExample()
        {
            yield return new object[] { new Film() { Title = "Title example" }, PropertyNameHelper.FilmTitleProperty };
            yield return new object[] { new Vehicle() { Name = "Name example" }, PropertyNameHelper.VehicleNameProperty };
            yield return new object[] { new Starship() { Name = "Name example" }, PropertyNameHelper.StarshipsNameProperty };
        }

        public static IEnumerable<object[]> TypesWithInCorrectPropertyExample()
        {
            yield return new object[] { new Film() { Title = "Title example" }, PropertyNameHelper.StarshipsNameProperty };
            yield return new object[] { new Vehicle() { Name = "Name example" }, PropertyNameHelper.FilmTitleProperty };
            yield return new object[] { new Starship() { Name = "Name example" }, "Test" };
        }
    }
}
