﻿using Microsoft.Extensions.Logging;
using Moq;
using SwapiApp.Interfaces;
using SwapiApp.Services;
using System;
using Xunit;

namespace SwapiApp.Tests
{
    public class FileServiceTests
    {
        private readonly IFileService _fileService;
        private readonly Mock<ILogger<FileService>> _loggerMock;

        public FileServiceTests()
        {
            _loggerMock = new Mock<ILogger<FileService>>();
            _fileService = new FileService(_loggerMock.Object);
        }

        [Fact]
        public void SaveAsJson_WhenGivenDataIsNull_ThenArgumentNullExceptionIsThrown()
        {
            Assert.Throws<ArgumentNullException>(() => _fileService.SaveAsJson(null, "test"));
        }

        [Theory]
        [ClassData(typeof(EmptyTextDataHelper))]
        public void SaveAsJson_WhenGivenFileNameIsEmpty_ThenArgumentExceptionIsThrown(string fileName)
        {
            Assert.Throws<ArgumentException>(() => _fileService.SaveAsJson("test", fileName));
        }
    }
}
