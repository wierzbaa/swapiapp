﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using SwapiApp.Interfaces;
using SwapiApp.Services;
using System.IO;
using SwapiApp.Models;
using Serilog;
using Microsoft.Extensions.Logging;
using System;

namespace SwapiApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var services = ConfigureServices();

            var serviceProvider = services.BuildServiceProvider();

            serviceProvider.GetService<IRunner>().Run();

            Console.ReadKey();
        }

        private static IServiceCollection ConfigureServices()
        {
            IServiceCollection services = new ServiceCollection();

            var config = LoadConfiguration();
            services.AddSingleton(config);

            services.Configure<SwapiConfiguration>(config.GetSection("SwapiConfiguration"));

            services.AddTransient<ISwapiService, SwapiService>();
            services.AddTransient<IHttpClientService, HttpClientService>();
            services.AddTransient<IFileService, FileService>();
            services.AddTransient<IRunner, Runner>();

            var serilogLogger = new LoggerConfiguration()
            .WriteTo.Console()
            .WriteTo.File(@"swapiapp_log.txt")
            .CreateLogger();

            services.AddLogging(builder =>
            {
                builder.SetMinimumLevel(LogLevel.Information);
                builder.AddSerilog(logger: serilogLogger, dispose: true);
            });

            return services;
        }

        private static IConfiguration LoadConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            return builder.Build();
        }
    }
}
