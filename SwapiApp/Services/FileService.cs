﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SwapiApp.Interfaces;
using System;
using System.IO;

namespace SwapiApp.Services
{
    public class FileService : IFileService
    {
        private readonly ILogger<FileService> _logger;
        private string _filePath = Directory.GetCurrentDirectory();

        public FileService(ILogger<FileService> logger)
        {
            _logger = logger;
        }

        public void SaveAsJson(object objToSave, string fileName)
        {
            _logger.LogInformation("Start writing data to file");

            if (objToSave == null)
            {
                throw new ArgumentNullException("There is no object to save");
            }

            if (string.IsNullOrWhiteSpace(fileName))
            {
                throw new ArgumentException("Filename cannot be null or empty!");
            }

            var path = Path.Combine(_filePath, fileName);

            using (StreamWriter file = File.CreateText(path))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, objToSave);
            }

            _logger.LogInformation($"The data was saved in file: {path}");
        }
    }
}
