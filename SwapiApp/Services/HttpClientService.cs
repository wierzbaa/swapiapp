﻿using Microsoft.Extensions.Options;
using SwapiApp.Interfaces;
using SwapiApp.Models;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace SwapiApp.Services
{
    public class HttpClientService : IHttpClientService
    {
        private readonly SwapiConfiguration _config;

        public HttpClientService(IOptionsMonitor<SwapiConfiguration> config)
        {
            _config = config.CurrentValue;
        }

        public async Task<T> GetAsync<T>(string uri)
        {
            using (var client = CreateClient())
            {
                var response = await client.GetAsync(uri);

                response.EnsureSuccessStatusCode();

                var result = await response.Content.ReadAsAsync<T>();

                return result;
            }
        }

        private HttpClient CreateClient()
        {
            var hostUri = new Uri(_config.BaseSwapiUrl);

            var httpClient = new HttpClient
            {
                BaseAddress = hostUri
            };

            return httpClient;
        }
    }
}
