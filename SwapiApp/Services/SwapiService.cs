﻿using Microsoft.Extensions.Logging;
using SwapiApp.Interfaces;
using SwapiApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SwapiApp.Services
{
    public class SwapiService : ISwapiService
    {
        private readonly IHttpClientService _httpClientService;
        private readonly ILogger _logger;

        public SwapiService(IHttpClientService httpClientService, ILogger<SwapiService> logger)
        {
            _logger = logger;
            _httpClientService = httpClientService;
        }

        public async Task<Person> GetDetailsByPersonName(string name)
        {
            var person = await GetPersonByName(name);

            return new Person()
            {
                Films = await GetNames<Film>(person.Films, PropertyNameHelper.FilmTitleProperty, "film"),
                StarShips = await GetNames<Starship>(person.Starships, PropertyNameHelper.StarshipsNameProperty, "starship"),
                Vehicles = await GetNames<Vehicle>(person.Vehicles, PropertyNameHelper.VehicleNameProperty, "vehicle")
            };
        }

        public async Task<People> GetPersonByName(string name)
        {
            _logger.LogInformation("Start retrieving a person's basic data");

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("Person name cannot be null or empty!");
            }

            var result = await _httpClientService.GetAsync<PeopleSearchResult>($"people/?search={name}");

            if (result == null || result.Results?.Count() == 0)
            {
                throw new Exception("There is no person with the given name");
            }

            _logger.LogInformation("Stop retrieving a person's basic data");

            return result.Results.First();
        }

        public async Task<List<string>> GetNames<T>(List<Uri> urls, string propertyName, string objectName)
        {
            _logger.LogInformation($"Start retrieving {objectName} names");

            var names = new List<string>();

            if (urls == null || !urls.Any())
            {
                return names;
            }

            foreach (var url in urls)
            {
                var result = await _httpClientService.GetAsync<T>(url.ToString());

                if (result == null)
                {
                    _logger.LogInformation($"No data available at {url}");
                }
                else
                {
                    if(!PropertyNameExist(result, propertyName))
                    {
                        throw new Exception($"Property name {propertyName} doesn't exist in given object");
                    }


                    names.Add(result.GetType().GetProperty(propertyName).GetValue(result, null).ToString());
                }
            }

            _logger.LogInformation($"Stop retrieving {objectName} names. {names.Count} objects found.");

            return names;
        }

        public bool PropertyNameExist(object obj, string propertyName)
        {
            if(obj.GetType().GetProperty(propertyName) != null)
            {
                return true;
            }

            return false;
        }
    }
}
