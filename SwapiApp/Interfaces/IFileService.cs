﻿namespace SwapiApp.Interfaces
{
    public interface IFileService
    {
        void SaveAsJson(object objToSave, string fileName);
    }
}
