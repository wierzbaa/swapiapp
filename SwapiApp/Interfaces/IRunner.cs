﻿using System.Threading.Tasks;

namespace SwapiApp.Interfaces
{
    public interface IRunner
    {
        Task Run();
    }
}
