﻿using SwapiApp.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SwapiApp.Interfaces
{
    public interface ISwapiService
    {
        Task<Person> GetDetailsByPersonName(string name);
        Task<People> GetPersonByName(string name);
        Task<List<string>> GetNames<T>(List<Uri> urls, string propertyName, string objectName);
        bool PropertyNameExist(object obj, string propertyName);
    }
}
