﻿using System.Threading.Tasks;

namespace SwapiApp.Interfaces
{
    public interface IHttpClientService
    {
        Task<T> GetAsync<T>(string uri);
    }
}
