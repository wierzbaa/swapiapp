﻿using Microsoft.Extensions.Logging;
using SwapiApp.Interfaces;
using System;
using System.Threading.Tasks;

namespace SwapiApp
{
    public class Runner : IRunner
    {
        private readonly ISwapiService _swapiService;
        private readonly IFileService _fileService;
        private readonly ILogger<Runner> _logger;
        private const string Name = "Luke Skywalker";
        private const string FileName = "SwapiAppData.txt";

        public Runner(ISwapiService swapiService, IFileService fileService, ILogger<Runner> logger)
        {
            _swapiService = swapiService;
            _fileService = fileService;
            _logger = logger;
        }

        public async Task Run()
        {
            try
            {
                var personDetails = await _swapiService.GetDetailsByPersonName(Name);
                _fileService.SaveAsJson(personDetails, FileName);
            }
            catch(Exception e)
            {
                _logger.LogError(e, "Application exception occurred");
            }
        }
    }
}
