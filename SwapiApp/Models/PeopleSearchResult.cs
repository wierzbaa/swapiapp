﻿using System;
using System.Collections.Generic;

namespace SwapiApp.Models
{
    public class PeopleSearchResult
    {
        public long Count { get; set; }
        public Uri Next { get; set; }
        public Uri Previous { get; set; }
        public List<People> Results { get; set; }
    }
}
