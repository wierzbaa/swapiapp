﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace SwapiApp.Models
{
    public class Film
    {
        public List<Uri> Characters { get; set; }
        public DateTime Created { get; set; }
        public string Director { get; set; }
        public DateTime Edited { get; set; }

        public List<Uri> Planets { get; set; }
        public string Producer { get; set; }
        public List<Uri> Species { get; set; }
        public List<Uri> Starships { get; set; }
        public string Title { get; set; }
        public Uri Url { get; set; }
        public List<Uri> Vehicles { get; set; }

        [JsonProperty("release_date")]
        public DateTime ReleaseDate { get; set; }


        [JsonProperty("opening_crawl")]
        public string OpeningCrawl { get; set; }


        [JsonProperty("episode_id")]
        public long EpisodeId { get; set; }
    }
}
