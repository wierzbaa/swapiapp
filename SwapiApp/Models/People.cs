﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace SwapiApp.Models
{
    public class People
    {
        [JsonProperty("birth_year")]
        public string BirthYear { get; set; }

        [JsonProperty("eye_color")]
        public string EyeColor { get; set; }

        [JsonProperty("hair_color")]
        public string HairColor { get; set; }

        [JsonProperty("skin_color")]
        public string SkinColor { get; set; }

        public List<Uri> Films { get; set; }
        public string Gender { get; set; }
        public string Height { get; set; }
        public Uri Homeworld { get; set; }
        public string Mass { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public DateTime Edited { get; set; }
        public List<Uri> Species { get; set; }
        public List<Uri> Starships { get; set; }
        public Uri Url { get; set; }
        public List<Uri> Vehicles { get; set; }
    }
}
