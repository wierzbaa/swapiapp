﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace SwapiApp.Models
{
    public class Vehicle
    {
        public string Consumables { get; set; }
        public DateTime Created { get; set; }
        public string Crew { get; set; }
        public DateTime Edited { get; set; }
        public string Length { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Name { get; set; }
        public string Passengers { get; set; }
        public object[] Pilots { get; set; }
        public List<Uri> Films { get; set; }
        public Uri Url { get; set; }

        [JsonProperty("vehicle_class")]
        public string VehicleClass { get; set; }

        [JsonProperty("cargo_capacity")]
        public string CargoCapacity { get; set; }

        [JsonProperty("cost_in_credits")]
        public string CostInCredits { get; set; }

        [JsonProperty("max_atmosphering_speed")]
        public string MaxAtmospheringSpeed { get; set; }
    }
}
