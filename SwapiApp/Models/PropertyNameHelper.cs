﻿namespace SwapiApp.Models
{
    public static class PropertyNameHelper
    {
        public const string FilmTitleProperty = "Title";
        public const string StarshipsNameProperty = "Name";
        public const string VehicleNameProperty = "Name";
    }
}
