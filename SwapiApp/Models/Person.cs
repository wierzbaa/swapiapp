﻿using System.Collections.Generic;

namespace SwapiApp.Models
{
    public class Person
    {
        public List<string> Films { get; set; }
        public List<string> Vehicles { get; set; }
        public List<string> StarShips { get; set; }
    }
}
